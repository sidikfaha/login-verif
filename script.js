/**
 * Création de l'objet input
 * @param param {Object} Id du input
 */
function $(p) {
    this.e = document.querySelector("#" + p.id);
    this.box = document.querySelector("#" + p.box) ? (p.box) : document.querySelector("#" + p.id + "_" + "box");
    this.err = p.errMsg ? (p.errMsg) : "Vous devez remplir ce champ...";
    this.ok = p.okMsg ? (p.okMsg) : "Ce champ est valide ! ";
};

$.prototype.getErr = function () {
    return this.err;
};

$.prototype.init = function () {
    console.log(this.e);
    console.log(this.box);
    console.log(this.err);
    console.log(this.ok);

    const err = this.err;
    const ok = this.ok;
    const e = this.e, box = this.box;
    this.e.addEventListener('keyup', function () {
        if (e.value != "") {
            box.innerText = ok;
            box.setAttribute('class', 'ibox ibox-success');
        } else {
            box.innerText = err;
            box.setAttribute('class', 'ibox ibox-danger');
        }
        box.style = 'display:block;transform:scale(1),rotate(0);';

    });
};


let username = new $({
    id: "username"
}).init();
